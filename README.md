# Actors sample Android application project #

It is an Android application sample which showcases an interpretation of a [clean](http://blog.8thlight.com/uncle-bob/2012/08/13/the-clean-architecture.html) way to architecting Android projects.

In this demo I don't use any framework to implement it, because of the purpose of it is to show you a little how I work.

Working that way I'm looking for achieve robust, maintainable, testable, and flexible and high quality software products.

And show a right way to work with a distributed revision control system using branches.

Done
----

* The objective of work with modules is the separation of concerns.
* Created network module to consume REST web service.
* Created components module with an default CardView.
* Created mobile module it's application core.
* Unit test implemented using TDD, regular Java objects as Parser class with JUnit.

Libraries
---------

 * Picasso - http://square.github.io/picasso

To Do
-----

 * Implement pagination (WIP)
 * Finish some points of the exercise.
 * Finish implementation of the Gateway pattern to test with mocks factories.
 * Functional testing demo with [Espresso](https://developer.android.com/intl/es/tools/testing-support-library/index.html#Espresso)
 * Clean activities/fragments to Presenters to implement MVP.


## Contact ##

**Author:** [Pol Gomez Guerrero](https://www.linkedin.com/pub/pol-g%C3%B3mez-guerrero/3a/918/453)

**Email:** polgomezguerrero@gmail.com

License
-------

    Copyright 2015 Pol Gómez Guerrero

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.