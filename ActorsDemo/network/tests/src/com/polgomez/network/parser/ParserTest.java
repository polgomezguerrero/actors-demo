package com.polgomez.network.parser;

import com.polgomez.network.gateway.http.model.HttpResponseModel;
import com.polgomez.network.model.Actor;
import com.polgomez.network.model.Actors;
import com.polgomez.network.model.Movie;
import junit.framework.*;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.*;

public class ParserTest extends TestCase {

    public void testParseHttpResponseWithNull() {
        HttpResponseModel responseModel = null;
        responseModel = Parser.parseHttpResponse(null);

        assertNotNull(responseModel);
    }

    public void testParseHttpResponseWithEmptyJsonObject() {
        HttpResponseModel responseModel = null;
        responseModel = Parser.parseHttpResponse(new JSONObject());

        assertNotNull(responseModel);
    }

    public void testParseActorsWithNull() {
        Actors actors = null;
        actors = Parser.parseActors(null);

        assertNotNull(actors);
    }

    public void testParseActorsWithEmptyJsonObject() {
        Actors actors = null;
        actors = Parser.parseActors(new JSONObject());

        assertNotNull(actors);
    }

    public void testParseActorWithNull() {
        Actor actor = null;
        actor = Parser.parseActor(null);

        assertNotNull(actor);
    }

    public void testParseActorWithEmptyJsonObject() {
        Actor actor = null;
        actor = Parser.parseActor(new JSONObject());

        assertNotNull(actor);
    }

    public void testParseMoviesWithNull() {
        List<Movie> movies = null;
        movies = Parser.parseMovies(null);

        assertNotNull(movies);
    }

    public void testParseMoviesWithEmptyJsonObject() {
        List<Movie> movies = null;
        movies = Parser.parseMovies(new JSONArray());

        assertNotNull(movies);
    }

    public void testParseMovieWithNull() {
        Movie movie = null;
        movie = Parser.parseMovie(null);

        assertNotNull(movie);
    }

    public void testParseMovieWithEmptyJsonObject() {
        Movie movie = null;
        movie = Parser.parseMovie(new JSONObject());

        assertNotNull(movie);
    }

}
