package com.polgomez.network.gateway;

import com.polgomez.network.form.PaginationForm;
import com.polgomez.network.model.Actors;

/**
 * <p><b>Gateway</b></p>
 *
 * Gateway pattern:
 * Class to encapsulate the access to external resources. It's a good solution to test purpose.
 */
public abstract class Gateway {

    /**
     * Call that returns all actors without pagination
     * @param listener
     * @param errorListener
     */
    public abstract void getActors(Response.Listener<Actors> listener,
            Response.ErrorListener errorListener);

    public abstract void getActors(PaginationForm form, Response.Listener<Actors> listener,
            Response.ErrorListener errorListener);
}
