package com.polgomez.network.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.*;

/**
 * <p><b>Actors data model.</b></p>
 */
public class Actors implements Parcelable {

    private List<Actor> data;
    private int totalElements;

    public Actors() {
    }

    public List<Actor> getData() {
        return data;
    }

    public void setData(List<Actor> data) {
        this.data = data;
    }

    public long getTotalElements() {
        return totalElements;
    }

    public void setTotalElements(int totalElements) {
        this.totalElements = totalElements;
    }

    @Override
    public String toString() {
        return "Actors{" +
                "data=" + data +
                ", totalElements=" + totalElements +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(data);
        dest.writeInt(this.totalElements);
    }

    private Actors(Parcel in) {
        if (data == null) {
            data = new ArrayList<>();
        }
        in.readList(data, Actor.class.getClassLoader());
        this.totalElements = in.readInt();
    }

    public static final Parcelable.Creator<Actors> CREATOR = new Parcelable.Creator<Actors>() {
        public Actors createFromParcel(Parcel source) {
            return new Actors(source);
        }

        public Actors[] newArray(int size) {
            return new Actors[size];
        }
    };
}
