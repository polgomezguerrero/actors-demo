package com.polgomez.network.controller;

/**
 * <p><b>ProgressController</b></p>
 */
public interface ProgressController {

    void hideProgress();

    void showProgress();

}
