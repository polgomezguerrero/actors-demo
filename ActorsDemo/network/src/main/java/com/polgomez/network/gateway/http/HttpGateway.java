package com.polgomez.network.gateway.http;

import android.os.AsyncTask;
import com.polgomez.network.api.Api;
import com.polgomez.network.controller.ErrorController;
import com.polgomez.network.controller.ProgressController;
import com.polgomez.network.form.PaginationForm;
import com.polgomez.network.gateway.Gateway;
import com.polgomez.network.gateway.Response;
import com.polgomez.network.gateway.http.client.HttpClient;
import com.polgomez.network.gateway.http.model.HttpResponseModel;
import com.polgomez.network.model.Actors;
import com.polgomez.network.parser.Parser;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.*;

/**
 * <p><B>HttpGateway</B></p>
 *
 *
 * Gateway that access to network data via http
 */
public class HttpGateway extends Gateway {

    private static final String STATUS_CODE_CORRECT = "200";

    private static final String ERROR_MESSAGE_PARSING_RESPONSE = "Error parsing response";
    private static final String RESPONSE_ERROR_STATUS_CODE = "ERROR - Response status code: ";
    private static final String ERROR = "ERROR";
    public static final String CANCELED = "Canceled";

    private final Api mApi;
    private ProgressController mProgressController;
    private ErrorController mErrorController;

    public HttpGateway(Api api) {
        mApi = api;
    }

    public void setProgressController(ProgressController progressController) {
        mProgressController = progressController;
    }

    public void setErrorController(ErrorController errorController) {
        mErrorController = errorController;
    }

    @Override
    public void getActors(final Response.Listener<Actors> listener,
            final Response.ErrorListener errorListener) {
        final HttpClient client = buildHttpClient();
        new HttpGatewayAsyncTask(mProgressController, mErrorController){
            @Override
            protected String doInBackground(Void... params) {
                return client.doConnection();
            }

            @Override
            protected void onPostExecute(String response) {
                super.onPostExecute(response);

                handleResponse(listener, errorListener, response);
            }
        }.execute();
    }

    @Override
    public void getActors(PaginationForm form, final Response.Listener<Actors> listener,
            final Response.ErrorListener errorListener) {
        final Map<String, String> hashMap = new HashMap();
        if (form != null) {
            hashMap.put(PaginationForm.PAGINATION_PAGE_PARAM, Integer.toString(form.getPage()));
        }

        final HttpClient client = buildHttpClient();
        new HttpGatewayAsyncTask(mProgressController, mErrorController){
            @Override
            protected String doInBackground(Void... params) {
                return client.doConnection(hashMap);
            }

            @Override
            protected void onPostExecute(String response) {
                super.onPostExecute(response);

                handleResponse(listener, errorListener, response);
            }
        }.execute();
    }

    private HttpClient buildHttpClient() {
        HttpClient.Builder builder = new HttpClient.Builder();
        builder.url(mApi.getActorsUrl()).requestMethod(HttpClient.RequestMethod.GET);
        return builder.build();
    }

    private void handleResponse(Response.Listener<Actors> listener,
            Response.ErrorListener errorListener, String response) {
        try {
            Actors actorsResponse = Parser.parseActors(new JSONObject(response));
            if (actorsResponse.getData() != null) {
                listener.onResponse(actorsResponse);
            } else {
                errorListener.onErrorResponse(ERROR);
            }
        } catch (JSONException e) {
            errorListener.onErrorResponse(ERROR_MESSAGE_PARSING_RESPONSE);
        }
    }

    private abstract class HttpGatewayAsyncTask extends AsyncTask<Void, Void, String> {

        private ProgressController mProgressController;
        private ErrorController mErrorController;

        public HttpGatewayAsyncTask(ProgressController progressController,
                ErrorController errorController) {
            mProgressController = progressController;
            mErrorController = errorController;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (mErrorController != null) {
                mErrorController.hide();
            }

            if (mProgressController != null) {
                mProgressController.showProgress();
            }

        }

        @Override
        protected void onCancelled(String errorMessage) {
            super.onCancelled(errorMessage);
            if (mProgressController != null) {
                mProgressController.hideProgress();
            }

            if (mErrorController != null) {
                mErrorController.showErrorMessage(errorMessage);
            }
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            if (mProgressController != null) {
                mProgressController.hideProgress();
            }

            if (mErrorController != null) {
                mErrorController.showErrorMessage(CANCELED);
            }
        }

        @Override
        protected void onPostExecute(String response) {
            super.onPostExecute(response);
            if (mProgressController != null) {
                mProgressController.hideProgress();
            }
        }
    }
}
