package com.polgomez.network.parser;

import com.polgomez.network.gateway.http.model.HttpResponseModel;
import com.polgomez.network.model.Actor;
import com.polgomez.network.model.Actors;
import com.polgomez.network.model.Movie;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.*;

public class Parser {

    // HttpResponseModel json fields name values
    private final static String STATUS_CODE = "statusCode";
    private final static String BODY = "body";
    private final static String HEADERS = "headers";

    // Actors json fields name values
    private final static String TOTAL_ELEMENTS = "totalElements";
    private final static String DATA = "data";

    // Actor json fields name values
    private final static String ADULT = "adult";
    private final static String DESCRIPTION = "description";
    private final static String IDENTIFIER = "identifier";
    private final static String KNOW_FOR = "know_for";
    private final static String LOCATION = "location";
    private final static String NAME = "name";
    private final static String POPULARITY = "popularity";
    private final static String PROFILE_PATH = "profile_path";
    private final static String TOP = "top";

    // Movie json fields name values
    private final static String BACKDROP_PATH = "backdrop_path";
    private final static String ID = "id";
    private final static String MEDIA_TYPE = "media_type";
    private final static String ORIGINAL_TITLE = "original_title";
    private final static String POSTER_PATH = "poster_path";
    private final static String RELEASE_DATE = "release_date";
    private final static String TITLE = "title";
    private final static String VIDEO = "video";
    private final static String VOTE_AVERAGE = "vote_average";
    private final static String VOTE_COUNT = "vote_count";

    public static HttpResponseModel parseHttpResponse(JSONObject jsonObject) {
        HttpResponseModel responseModel = new HttpResponseModel();
        if (jsonObject != null) {
            responseModel.setStatusCode(jsonObject.optString(STATUS_CODE));
            responseModel.setBody(parseActors(jsonObject.optJSONObject(BODY)));
            responseModel.setHeaders(jsonObject.optString(HEADERS));
        }

        return responseModel;
    }

    public static Actors parseActors(JSONObject jsonObject) {
        Actors actors = new Actors();
        if (jsonObject != null) {
            actors.setTotalElements(jsonObject.optInt(TOTAL_ELEMENTS));
            JSONArray data = jsonObject.optJSONArray(DATA);
            List<Actor> actorsList = new ArrayList();
            if (data != null) {
                int length = data.length();
                for (int i = 0; i < length; i++) {
                    actorsList.add(parseActor(data.optJSONObject(i)));
                }
            }
            actors.setData(actorsList);
        }

        return actors;
    }

    public static Actor parseActor(JSONObject jsonObject) {
        Actor actor = new Actor();
        if (jsonObject != null) {
            actor.setAdult(jsonObject.optBoolean(ADULT));
            actor.setDescription(jsonObject.optString(DESCRIPTION));
            actor.setIdentifier(jsonObject.optInt(IDENTIFIER));
            actor.setKnow_for(parseMovies(jsonObject.optJSONArray(KNOW_FOR)));
            actor.setLocation(jsonObject.optString(LOCATION));
            actor.setName(jsonObject.optString(NAME));
            actor.setPopularity(jsonObject.optDouble(POPULARITY));
            actor.setProfile_path(jsonObject.optString(PROFILE_PATH));
            actor.setTop(jsonObject.optBoolean(TOP));
        }

        return actor;
    }

    public static List<Movie> parseMovies(JSONArray jsonArray) {
        List<Movie> movies = new ArrayList<>();
        if (jsonArray != null) {
            int length = jsonArray.length();
            for (int i = 0; i < length; i++) {
                movies.add(parseMovie(jsonArray.optJSONObject(i)));
            }
        }

        return movies;
    }

    public static Movie parseMovie(JSONObject jsonObject) {
        Movie movie = new Movie();
        if (jsonObject != null) {
            movie.setAdult(jsonObject.optBoolean(ADULT));
            movie.setBackdrop_path(jsonObject.optString(BACKDROP_PATH));
            movie.setDescription(jsonObject.optString(DESCRIPTION));
            movie.setId(jsonObject.optLong(ID));
            movie.setMedia_type(jsonObject.optString(MEDIA_TYPE));
            movie.setOriginal_title(jsonObject.optString(ORIGINAL_TITLE));
            movie.setPopularity(jsonObject.optDouble(POPULARITY));
            movie.setPoster_path(jsonObject.optString(POSTER_PATH));
            movie.setRelease_date(jsonObject.optLong(RELEASE_DATE));
            movie.setTitle(jsonObject.optString(TITLE));
            movie.setVideo(jsonObject.optBoolean(VIDEO));
            movie.setVote_average(jsonObject.optDouble(VOTE_AVERAGE));
            movie.setVote_count(jsonObject.optInt(VOTE_COUNT));
        }

        return movie;
    }

}
