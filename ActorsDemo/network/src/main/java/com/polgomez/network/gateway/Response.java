package com.polgomez.network.gateway;

/**
 * <p><b>GatewayResponse</b></p>
 *
 * A generic response listener to use by any gateway that recovers model data from any source.
 */
public class Response {


    public interface Listener<T> {
        /** Called when a response is received. */
        void onResponse(T response);
    }

    public interface ErrorListener {
        /**
         * Callback method that an error has been occurred with the user-readable message.
         */
        void onErrorResponse(String errorMessage);
    }
}
