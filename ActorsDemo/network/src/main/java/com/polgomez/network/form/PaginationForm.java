package com.polgomez.network.form;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * <p><b>PaginationModel</b></p>
 *
 * Class to perform pagination
 */
public class PaginationForm implements Parcelable {

    public static String PAGINATION_PAGE_PARAM = "page";

    int page;

    public PaginationForm() {
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    @Override
    public String toString() {
        return "PaginationModel{" +
                "page=" + page +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.page);
    }

    private PaginationForm(Parcel in) {
        this.page = in.readInt();
    }

    public static final Parcelable.Creator<PaginationForm> CREATOR = new Parcelable.Creator<PaginationForm>() {
        public PaginationForm createFromParcel(Parcel source) {
            return new PaginationForm(source);
        }

        public PaginationForm[] newArray(int size) {
            return new PaginationForm[size];
        }
    };
}
