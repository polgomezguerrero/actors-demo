package com.polgomez.network.gateway.http.client;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.*;

/**
 * <p><b>HttpClient</b></p>
 *
 * Simple implementation of a Client to make connections via HTTP,
 *
 *
 * - No dependencies to third libraries
 * - Builder pattern demo
 * - SOLID
 */
public class HttpClient {

    /** The default encoding fot the parameters of a GET request. */
    private static final String DEFAULT_GET_PARAMS_ENCODING = "UTF-8";

    private static final int READ_TIMEOUT_MILLIS = 10000;
    private static final int CONNECT_TIMEOUT_MILLIS = 15000;

    private final String mUrl;
    private final RequestMethod mRequestMethod;

    public HttpClient(Builder builder) {
        this.mUrl = builder.url;
        this.mRequestMethod = builder.requestMethod;
    }

    public String doConnection() {
        return doConnection(null);
    }

    public String doConnection(Map<String, String> params) {
        String urlString = mUrl;
        String response;

        if (params != null) {
            urlString = encodeGetParameters(urlString, params);
        }

        try {
            URL url = new URL(urlString);
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            httpURLConnection.setReadTimeout(READ_TIMEOUT_MILLIS);
            httpURLConnection.setConnectTimeout(CONNECT_TIMEOUT_MILLIS);
            httpURLConnection.setRequestMethod(mRequestMethod.value());

            httpURLConnection.connect();

            InputStream inputStream = httpURLConnection.getInputStream();
            BufferedReader r = new BufferedReader(new InputStreamReader(inputStream));
            StringBuilder stringBuilder = new StringBuilder();
            String line;
            while ((line = r.readLine()) != null) {
                stringBuilder.append(line);
            }

            response = stringBuilder.toString();

        } catch (Exception e) {
            e.printStackTrace();
            response = e.getMessage();
        }

        return response;
    }

    /**
     * Receive a map of key=value parameters and add them to the url as a query string. The
     * parameters are url encoded.
     *
     * @param url the url of the request.
     * @param params the map of parameters with key, value pairs.
     *
     * @return the url with the added parameters as query string.
     */
    protected String encodeGetParameters(String url, Map<String, String> params) {
        StringBuilder encodedParams = new StringBuilder();
        String encoding = DEFAULT_GET_PARAMS_ENCODING;

        try {
            for (Map.Entry<String, String> entry : params.entrySet()) {
                encodedParams.append(URLEncoder.encode(entry.getKey(), encoding));
                encodedParams.append('=');
                encodedParams.append(URLEncoder.encode(entry.getValue(), encoding));
                encodedParams.append('&');
            }
            url += "?" + encodedParams.toString();
        } catch (UnsupportedEncodingException uee) {
            throw new RuntimeException("Encoding not supported: " + encoding, uee);
        }

        return url;
    }

    public static class Builder {

        private String url;
        private RequestMethod requestMethod;

        public Builder() {

        }

        public Builder(String url, RequestMethod requestMethod) {
            this.url = url;
            this.requestMethod = requestMethod;
        }

        public Builder url(String url) {
            this.url = url;
            return this;
        }

        public Builder requestMethod(RequestMethod requestMethod) {
            this.requestMethod = requestMethod;
            return this;
        }

        public HttpClient build() {
            return new HttpClient(this);
        }
    }

    public enum RequestMethod {
        GET("GET"), POST("POST"), PUT("PUT");

        private final String method;

        RequestMethod(String method) {
            this.method = method;
        }

        public String value() {
            return method;
        }
    }
}
