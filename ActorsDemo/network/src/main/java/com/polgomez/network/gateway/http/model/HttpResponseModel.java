package com.polgomez.network.gateway.http.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.polgomez.network.model.Actors;

public class HttpResponseModel implements Parcelable {

    private String statusCode;
    private Actors body;
    private String headers;

    public HttpResponseModel() {
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public Actors getBody() {
        return body;
    }

    public void setBody(Actors body) {
        this.body = body;
    }

    public String getHeaders() {
        return headers;
    }

    public void setHeaders(String headers) {
        this.headers = headers;
    }

    @Override
    public String toString() {
        return "HttpResponseModel{" +
                "statusCode='" + statusCode + '\'' +
                ", body=" + body +
                ", headers='" + headers + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.statusCode);
        dest.writeParcelable(this.body, 0);
        dest.writeString(this.headers);
    }

    private HttpResponseModel(Parcel in) {
        this.statusCode = in.readString();
        this.body = in.readParcelable(Actors.class.getClassLoader());
        this.headers = in.readString();
    }

    public static final Parcelable.Creator<HttpResponseModel> CREATOR = new Parcelable.Creator<HttpResponseModel>() {
        public HttpResponseModel createFromParcel(Parcel source) {
            return new HttpResponseModel(source);
        }

        public HttpResponseModel[] newArray(int size) {
            return new HttpResponseModel[size];
        }
    };
}
