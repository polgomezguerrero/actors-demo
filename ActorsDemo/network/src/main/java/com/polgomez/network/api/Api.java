package com.polgomez.network.api;

/**
 * <p><b>Api</b></p>
 */
public abstract class Api {

    public abstract String getActorsUrl();
}
