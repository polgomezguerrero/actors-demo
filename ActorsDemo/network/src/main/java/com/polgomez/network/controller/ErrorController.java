package com.polgomez.network.controller;

public interface ErrorController {

    void showErrorMessage(String errorMessage);

    void hide();

}
