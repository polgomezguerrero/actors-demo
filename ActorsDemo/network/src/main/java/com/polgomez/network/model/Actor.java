package com.polgomez.network.model;


import android.os.Parcel;
import android.os.Parcelable;

import java.util.*;

/**
 * <p><b>Actor data model</b></p>
 */
public class Actor implements Parcelable {

    private boolean adult;
    private String description;
    private int identifier;
    private List<Movie> know_for;
    private String location;
    private String name;
    private double popularity;
    private String profile_path;
    private boolean top;

    public Actor() {
    }

    public boolean isAdult() {
        return adult;
    }

    public void setAdult(boolean adult) {
        this.adult = adult;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getIdentifier() {
        return identifier;
    }

    public void setIdentifier(int identifier) {
        this.identifier = identifier;
    }

    public List<Movie> getKnow_for() {
        return know_for;
    }

    public void setKnow_for(List<Movie> know_for) {
        this.know_for = know_for;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPopularity() {
        return popularity;
    }

    public void setPopularity(double popularity) {
        this.popularity = popularity;
    }

    public String getProfile_path() {
        return profile_path;
    }

    public void setProfile_path(String profile_path) {
        this.profile_path = profile_path;
    }

    public boolean isTop() {
        return top;
    }

    public void setTop(boolean top) {
        this.top = top;
    }

    @Override
    public String toString() {
        return "Actor{" +
                "adult=" + adult +
                ", description='" + description + '\'' +
                ", identifier=" + identifier +
                ", know_for=" + know_for +
                ", location='" + location + '\'' +
                ", name='" + name + '\'' +
                ", popularity=" + popularity +
                ", profile_path='" + profile_path + '\'' +
                ", top=" + top +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte(adult ? (byte) 1 : (byte) 0);
        dest.writeString(this.description);
        dest.writeInt(this.identifier);
        dest.writeList(this.know_for);
        dest.writeString(this.location);
        dest.writeString(this.name);
        dest.writeDouble(this.popularity);
        dest.writeString(this.profile_path);
        dest.writeByte(top ? (byte) 1 : (byte) 0);
    }

    private Actor(Parcel in) {
        this.adult = in.readByte() != 0;
        this.description = in.readString();
        this.identifier = in.readInt();
        if (this.know_for == null) {
            this.know_for = new ArrayList();
        }
        in.readList(this.know_for, Movie.class.getClassLoader());
        this.location = in.readString();
        this.name = in.readString();
        this.popularity = in.readDouble();
        this.profile_path = in.readString();
        this.top = in.readByte() != 0;
    }

    public static final Parcelable.Creator<Actor> CREATOR = new Parcelable.Creator<Actor>() {
        public Actor createFromParcel(Parcel source) {
            return new Actor(source);
        }

        public Actor[] newArray(int size) {
            return new Actor[size];
        }
    };
}
