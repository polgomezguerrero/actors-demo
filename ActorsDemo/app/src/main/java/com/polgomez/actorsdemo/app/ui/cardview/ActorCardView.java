package com.polgomez.actorsdemo.app.ui.cardview;

import com.polgomez.components.cardview.DefaultCardView;
import com.polgomez.network.model.Actor;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * <p><b>ActorCardView</b></p>
 */
public class ActorCardView extends DefaultCardView {

    private final Actor mActor;

    public ActorCardView(Actor actor) {
        mActor = actor;
    }

    @Override
    public int getId() {
        return mActor.getIdentifier();
    }

    @Override
    public String getImage() {
        return mActor.getProfile_path();
    }

    @Override
    public String getName() {
        return mActor.getName();
    }

    @Override
    public String getLocation() {
        return mActor.getLocation();
    }

    @Override
    public String getDescription() {
        return mActor.getDescription();
    }

    @Override
    public String getRate() {
        return round(mActor.getPopularity(), 1);
    }

    /**
     * Method to round doubles
     *
     * @param value double to round
     * @param places decimals to show
     * @return an String with double value rounded to indicated decimals
     */
    private String round(double value, int places) {
        if (places < 0){
            throw new IllegalArgumentException();
        }

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return Double.toString(bd.doubleValue());
    }
}
