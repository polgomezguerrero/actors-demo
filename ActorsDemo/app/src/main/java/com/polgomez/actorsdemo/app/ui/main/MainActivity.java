package com.polgomez.actorsdemo.app.ui.main;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import com.polgomez.actorsdemo.R;
import com.polgomez.actorsdemo.app.ActorsDemoApplication;
import com.polgomez.actorsdemo.app.ui.ControllersManager;
import com.polgomez.actorsdemo.app.ui.DefaultErrorController;
import com.polgomez.actorsdemo.app.ui.DefaultProgressController;
import com.polgomez.actorsdemo.navigation.NavigationController;
import com.polgomez.network.controller.ErrorController;
import com.polgomez.network.controller.ProgressController;
import com.polgomez.network.gateway.http.HttpGateway;

/**
 * <p><b>MainActivity</b></p>
 *
 * Activity that contains all fragments
 *
 */
public class MainActivity extends AppCompatActivity implements ControllersManager {

    private DefaultProgressController mProgressController;
    private ErrorController mErrorController;
    private Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        mToolbar = (Toolbar) findViewById(R.id.view_toolbar);
        if (mToolbar != null) {
            setSupportActionBar(mToolbar);
        }

        if (savedInstanceState == null) {
            NavigationController.navigateToFragment(this, RecyclerListFragment.newInstance(),
                    R.id.fragment_container, false);
        }

        initialize();
    }

    private void initialize() {
        buildControllers();
    }

    private void buildControllers() {
        mProgressController = new DefaultProgressController(this);
        mErrorController = new DefaultErrorController(this);

        HttpGateway gateway = (HttpGateway) ActorsDemoApplication.getGateway();
        gateway.setErrorController(mErrorController);
        gateway.setProgressController(mProgressController);
    }

    @Override
    public ProgressController getProgressController() {
        return mProgressController;
    }

    @Override
    public ErrorController getErrorController() {
        return mErrorController;
    }
}
