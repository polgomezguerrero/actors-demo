package com.polgomez.actorsdemo.app.ui;

import android.support.v4.app.Fragment;

/**
 * <p><b><BaseFragment/b></p>
 */
public class BaseFragment extends Fragment {

    private final String mIdString;

    public BaseFragment() {
        Class fragmentClass = ((Object) this).getClass();
        mIdString = fragmentClass.getName();
    }

    public String getIdString() {
        return mIdString;
    }
}
