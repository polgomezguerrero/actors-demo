package com.polgomez.actorsdemo.network.api;

import com.polgomez.network.api.Api;

/**
 * <p><b>ActorsDemoApi</b></p>
 */
public class ActorsDemoApi extends Api {

    private static final String REST_ACTORS = "http://test.gotouche.com/rest/actors";

    public static ActorsDemoApi getInstance() {
        return new ActorsDemoApi();
    }

    @Override
    public String getActorsUrl() {
        return REST_ACTORS;
    }
}
