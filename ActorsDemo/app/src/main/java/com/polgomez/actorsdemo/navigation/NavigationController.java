package com.polgomez.actorsdemo.navigation;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import com.polgomez.actorsdemo.app.ui.BaseFragment;

/**
 * <p><b>NavigationController</b></p>
 *
 * Class to manage the flow/navigation of this application, between activities or manage the
 * fragments
 */
public class NavigationController {

    public static void navigateToFragment(FragmentActivity activity, Fragment fragment,
            int contentFrame, boolean addToBackStack) {
        pushFragment(activity, fragment, contentFrame, addToBackStack);
    }

    private static void pushFragment(FragmentActivity activity, Fragment fragment, int contentFrame,
            boolean addToBackStack) {
        if (fragment == null) {
            return;
        }

        FragmentTransaction fragmentTransaction = activity.getSupportFragmentManager()
                .beginTransaction();

        String tag;
        if (fragment instanceof BaseFragment) {
            tag = ((BaseFragment) fragment).getIdString();
        } else {
            tag = ((Object) fragment).getClass().getName();
        }

        fragmentTransaction.replace(contentFrame, fragment, tag);

        if (addToBackStack) {
            fragmentTransaction.addToBackStack(tag);
        }

        fragmentTransaction.commit();
        // Only calling executePendingTransactions() if no nested Fragment call
        if (contentFrame <= 0) {
            activity.getSupportFragmentManager().executePendingTransactions();
        }
    }

}
