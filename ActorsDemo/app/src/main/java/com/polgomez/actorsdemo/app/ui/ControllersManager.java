package com.polgomez.actorsdemo.app.ui;

import com.polgomez.network.controller.ErrorController;
import com.polgomez.network.controller.ProgressController;

/**
 * <p><b>ControllersManager</b></p>
 */
public interface ControllersManager {

    ProgressController getProgressController();

    ErrorController getErrorController();

}
