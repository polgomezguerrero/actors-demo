package com.polgomez.actorsdemo.app.ui;

import android.app.Activity;
import android.view.View;
import com.polgomez.actorsdemo.R;
import com.polgomez.network.controller.ProgressController;

/**
 * <p><b>DefaultProgressController</b></p>
 *
 * Class to manage default progress views
 */
public class DefaultProgressController implements ProgressController {

    private final View mProgressView;

    public DefaultProgressController(Activity activity) {
        mProgressView = activity.findViewById(R.id.default_progress);
    }

    @Override
    public void hideProgress() {
        if (mProgressView != null) {
            mProgressView.setVisibility(View.GONE);
        }
    }

    @Override
    public void showProgress() {
        if (mProgressView != null) {
            mProgressView.setVisibility(View.VISIBLE);
        }
    }
}
