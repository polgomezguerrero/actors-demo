package com.polgomez.actorsdemo.app.ui.main;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.polgomez.actorsdemo.R;
import com.polgomez.actorsdemo.app.ActorsDemoApplication;
import com.polgomez.actorsdemo.app.ui.ControllersManager;
import com.polgomez.actorsdemo.app.ui.cardview.ActorCardView;
import com.polgomez.components.cardview.DefaultCardView;
import com.polgomez.components.cardview.adapter.ActorCardViewRecyclerAdapter;
import com.polgomez.network.gateway.Response;
import com.polgomez.network.model.Actor;
import com.polgomez.network.model.Actors;

import java.util.*;

/**
 * <p><b>RecyclerListFragment</b></p>
 */
public class RecyclerListFragment extends Fragment implements View.OnClickListener {

    private LinearLayoutManager mLayoutManager;
    private RecyclerView mRecyclerView;

    private ActorCardViewRecyclerAdapter mAdapter;

    private Actors mActors;
    private List<DefaultCardView> mActorsCards;

    public static RecyclerListFragment newInstance() {
        return new RecyclerListFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        mRecyclerView = (RecyclerView)inflater.inflate(R.layout.fragment_recycler_list, container,
                false);

        configureView();

        if (savedInstanceState == null) {
            showProgress();
        } else {
            // TODO restore values MEMENTO
            // mActors = ...
        }

        return mRecyclerView;
    }

    private void showProgress() {
        Activity activity = getActivity();
        if (activity instanceof ControllersManager) {
            ((ControllersManager) activity).getProgressController().showProgress();
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if (mActors == null) {
            obtainData();
        } else {
            loadData();
        }
    }

    private void obtainData() {

        ActorsDemoApplication.getGateway().getActors(new Response.Listener<Actors>() {
            @Override
            public void onResponse(Actors response) {
                mActors = response;
                loadData();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(String errorMessage) {
                Activity activity = getActivity();
                if (activity instanceof ControllersManager) {
                    ((ControllersManager) activity).getErrorController()
                            .showErrorMessage(errorMessage);
                }
            }
        });
    }

    private void loadData() {
        if (mActorsCards == null) {
            mActorsCards = new ArrayList<>();
        }
        for (Actor actor : mActors.getData()) {
            mActorsCards.add(new ActorCardView(actor));
        }

        mAdapter = new ActorCardViewRecyclerAdapter(mActorsCards, this);
        mRecyclerView.setAdapter(mAdapter);
    }

    private void configureView() {
        // this setting improve performance
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
    }

    @Override
    public void onClick(View v) {
        // TODO handle onclick event
    }
}
