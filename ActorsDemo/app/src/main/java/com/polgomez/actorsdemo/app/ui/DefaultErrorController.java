package com.polgomez.actorsdemo.app.ui;

import android.app.Activity;
import android.view.View;
import android.widget.TextView;
import com.polgomez.actorsdemo.R;
import com.polgomez.network.controller.ErrorController;

public class DefaultErrorController implements ErrorController {

    private final View mDefaultErrorContainer;
    private final TextView mErrorTextView;

    public DefaultErrorController(Activity activity) {
        mDefaultErrorContainer = activity.findViewById(R.id.default_error_container);
        mErrorTextView = (TextView) activity.findViewById(R.id.default_error_text);
    }

    @Override
    public void showErrorMessage(String errorMessage) {
        mErrorTextView.setText(errorMessage);
        mDefaultErrorContainer.setVisibility(View.VISIBLE);
    }

    @Override
    public void hide() {
        mDefaultErrorContainer.setVisibility(View.GONE);
    }
}
