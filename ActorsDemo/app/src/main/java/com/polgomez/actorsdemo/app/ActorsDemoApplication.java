package com.polgomez.actorsdemo.app;

import android.app.Application;
import com.polgomez.actorsdemo.network.api.ActorsDemoApi;
import com.polgomez.network.gateway.Gateway;
import com.polgomez.network.gateway.http.HttpGateway;

/**
 * <p><b>ActorsDemoApplication</b></p>
 */
public class ActorsDemoApplication extends Application {

    private static HttpGateway mGateway;

    @Override
    public void onCreate() {
        super.onCreate();

        mGateway = new HttpGateway(ActorsDemoApi.getInstance());
    }

    public static Gateway getGateway() {
        return mGateway;
    }
}
