package com.polgomez.components.cardview;

import android.content.Context;
import android.content.res.Resources;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.polgomez.components.R;
import com.polgomez.components.transformation.CircleTransformation;
import com.squareup.picasso.Picasso;

/**
 * <p><b>CardView</b></p>
 */
public abstract class DefaultCardView {

    public abstract int getId();

    public abstract String getImage();

    public abstract String getName();

    public abstract String getLocation();

    public abstract String getDescription();

    public abstract String getRate();

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private CardView mCardView;
        private TextView mName;
        private ImageView mImage;
        private ImageView mLocationIcon;
        private TextView mLocation;
        private TextView mDescription;
        private TextView mRate;

        public ViewHolder(View view) {
            super(view);

            if (view instanceof CardView) {
                mCardView = (CardView) view;
                mName = (TextView) view.findViewById(R.id.actor_card_name_tv);
                mImage = (ImageView) view.findViewById(R.id.actor_card_image_iv);
                mLocationIcon = (ImageView) view.findViewById(R.id.actor_card_location_drawable_iv);
                mLocation = (TextView) view.findViewById(R.id.actor_card_location_tv);
                mDescription = (TextView) view.findViewById(R.id.actor_card_description_tv);
                mRate = (TextView) view.findViewById(R.id.actor_card_rate_tv);
            }
        }

        public void fillViews(DefaultCardView defaultCardView) {
            mName.setText(defaultCardView.getName());
            mLocation.setText(defaultCardView.getLocation());
            mDescription.setText(defaultCardView.getDescription());
            mRate.setText(defaultCardView.getRate());
        }

        public void loadImage(DefaultCardView defaultCardView) {
            Picasso.with(mImage.getContext())
                    .load(defaultCardView.getImage())
                    .placeholder(R.drawable.ic_person_black)
                    .transform(new CircleTransformation())
                    .into(mImage);
        }

        public void loadCardColors(boolean isLight) {
            Context context = mCardView.getContext();

            int cardBackgroundResourceId;
            int cardTextColorResourceId;
            int cardLocationIconResourceId;

            if (isLight) {
                cardBackgroundResourceId = ContextCompat.getColor(context, android.R.color.white);
                cardTextColorResourceId = ContextCompat.getColor(context, android.R.color.black);
                cardLocationIconResourceId = R.drawable.pin_black;
            } else {
                cardBackgroundResourceId = ContextCompat.getColor(context, android.R.color.black);
                cardTextColorResourceId = ContextCompat.getColor(context, android.R.color.white);
                cardLocationIconResourceId = R.drawable.pin_white;
            }
            loadResources(cardBackgroundResourceId, cardTextColorResourceId,
                    cardLocationIconResourceId);
        }

        private void loadResources(int backgroundResourceId, int textColorResourceId, int
                iconResourceId) {
            mCardView.setCardBackgroundColor(backgroundResourceId);

            mName.setTextColor(textColorResourceId);
            mLocation.setTextColor(textColorResourceId);
            mDescription.setTextColor(textColorResourceId);

            mLocationIcon.setImageResource(iconResourceId);
        }
    }

}
