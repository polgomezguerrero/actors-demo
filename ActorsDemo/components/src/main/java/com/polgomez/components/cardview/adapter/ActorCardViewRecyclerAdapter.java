package com.polgomez.components.cardview.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.polgomez.components.R;
import com.polgomez.components.cardview.DefaultCardView;

import java.util.*;

/**
 * <p><b>ActorCardViewRecyclerAdapter</b></p>
 */
public class ActorCardViewRecyclerAdapter extends RecyclerView.Adapter<DefaultCardView.ViewHolder>{

    private static final int VIEW_TYPE_DEFAULT = 1;
    private static final int VIEW_TYPE_HEADER = 2;

    private final List<DefaultCardView> mCards;
    private final View.OnClickListener mOnClickListener;

    public ActorCardViewRecyclerAdapter(List<DefaultCardView> cards,
            View.OnClickListener onClickListener) {
        mCards = cards;
        mOnClickListener = onClickListener;
    }

    @Override
    public DefaultCardView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        boolean isDefaultType = viewType == VIEW_TYPE_DEFAULT;
        int viewResource = isDefaultType ? R.layout.actor_card :
                           R.layout.empty_view;

        View simpleCardView = LayoutInflater.from(parent.getContext()).inflate(
                viewResource, parent, false);

        if (isDefaultType) {
            simpleCardView.setOnClickListener(mOnClickListener);
        }

        return new DefaultCardView.ViewHolder(simpleCardView);
    }

    @Override
    public void onBindViewHolder(DefaultCardView.ViewHolder holder, int position) {
        if (position != 0) { // Because first position is the empty header
            int location = position - 1;
            boolean isLight = position % 2 == 0;
            DefaultCardView viewModel = mCards.get(location);
            holder.fillViews(viewModel);
            holder.loadImage(viewModel);
            holder.loadCardColors(isLight);
        }
    }

    @Override
    public int getItemCount() {
        return mCards == null ? 0 : mCards.size();
    }

    @Override
    public int getItemViewType(int position) {
        return position == 0 ? VIEW_TYPE_HEADER : VIEW_TYPE_DEFAULT;
    }
}
